import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {
  
  // email: string;
  // password: string;
  public usuario;

  constructor(private router: Router) {   
    this.usuario = {
      "email": ""
  };
  }

  ngOnInit() {
  }

  login(){
    console.log('voy ');
    this.router.navigate(['']);
  }

}
