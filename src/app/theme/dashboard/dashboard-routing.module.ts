import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from '../../guard/login.guard';

const routes: Routes = [
  {
    path: '',    
    data: {
      title: 'Dashboard',
      status: false
    },
    children: [
      {
        path: 'default',
        canActivateChild: [LoginGuard],
        loadChildren: './default/default.module#DefaultModule'
      },
      {
        path: 'ecommerce',
        canActivateChild: [LoginGuard],
        loadChildren: './ecommerce/ecommerce.module#EcommerceModule'
      },
      {
        path: 'ubicacion',
        canActivateChild: [LoginGuard],
        loadChildren: './ubicacion/ubicacion.module#UbicacionModule'
      },
      {
        path: 'users',
        canActivateChild: [LoginGuard],
        loadChildren: './users/users.module#UsersModule'
      },
      {
        path: 'employees',
        canActivateChild: [LoginGuard],
        loadChildren: './employees/employees.module#EmployeesModule'
      },      
      {
        path: 'analytics',
        loadChildren: './analytics/analytics.module#AnalyticsModule'
      },
      {
        path: 'forms',
        canActivateChild: [LoginGuard],
        loadChildren: './analytics/analytics.module#AnalyticsModule'
      }      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
