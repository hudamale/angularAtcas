import { Component, OnInit } from '@angular/core';
import { DatosService } from "../default/datos.service";
import { AdminComponent } from '../../../layout/admin/admin.component';

declare const AmCharts: any;

import '../../../../assets/charts/amchart/amcharts.js';
import '../../../../assets/charts/amchart/gauge.js';
import '../../../../assets/charts/amchart/pie.js';
import '../../../../assets/charts/amchart/serial.js';
import '../../../../assets/charts/amchart/light.js';
import '../../../../assets/charts/amchart/ammap.js';
import '../../../../assets/charts/amchart/worldLow.js';
import '../../../../assets/charts/amchart/continentsLow.js';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import "rxjs/add/operator/map";

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: [
    './analytics.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class AnalyticsComponent implements OnInit {
  powerCardData: any;
  powerCardOption: any;

  waterCardData: any;
  waterCardOption: any;

  energyCardData: any;
  energyCardOption: any;

  public amountCardData: any;
  public amountCardOption: any;
  public sessionId: string;
  public dataUri;
  public findUser: any;
  
  constructor(private router: Router, 
              private http: HttpClient, 
              private _datos: DatosService, 
              private adminLayout: AdminComponent
            ) { }

  ngOnInit() {    
    this.adminLayout.toggleOpenedoffcan();
    this.adminLayout.onClearUser(); 
    localStorage.setItem('userId', null);
  }

  change(){
    this.findUser = 0;
  }

  login(form: NgForm){
    this.findUser = 0;
    if(form.value.email.length < 1){
      this.findUser = 2;
    }else{ 
      this.findUser = 1;
      localStorage.setItem('userId', form.value.email);
      this._datos.getUserName(localStorage.getItem('userId')).subscribe(
        res => {
          this.findUser = res;
          if(this.findUser.length>0){            
            this.router.navigate(['dashboard/ecommerce']);  
            this.adminLayout.onUserMenu();
          }else{
            this.findUser = 2;
            localStorage.clear();
          }
        }
      )       
    } 
  }

  mostrar(){
    this.sessionId = localStorage.getItem('userId');
    console.log(this.sessionId);
  }  

}


