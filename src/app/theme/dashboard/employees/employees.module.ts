import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './employees.component';
import {EmployeesRoutingModule} from './employees-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';
import {FormsModule} from '@angular/forms';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {AgmCoreModule} from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [  
    CommonModule,
    EmployeesRoutingModule,
    SharedModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2GoogleChartsModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk'})    
  ],
  declarations: [EmployeesComponent],
  bootstrap: [EmployeesComponent]
})
export class EmployeesModule { }
