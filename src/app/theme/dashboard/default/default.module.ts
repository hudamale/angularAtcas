import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import {DefaultRoutingModule} from './default-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';
import {FormsModule} from '@angular/forms';
import {DatosService} from './datos.service';

@NgModule({
  imports: [  
    CommonModule,
    DefaultRoutingModule,
    SharedModule,
    ChartModule,
    FormsModule
  ],
  declarations: [DefaultComponent],
  bootstrap: [DefaultComponent]
})
export class DefaultModule { }
