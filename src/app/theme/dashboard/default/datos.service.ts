import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import "rxjs/add/operator/map";

@Injectable()

export class DatosService {

  constructor(private _http: HttpClient) {}

    getDatos(){
      return this._http.get('http://localhost:3000/users');
    }

    getUser(){
      const urlDataUser = "http://192.168.2.154:3000/users";
      return this._http.get(urlDataUser).map(res=>res);
    }

    getUserName(name: string){
      const urlDataUserName = "http://192.168.2.154:3000/user/"+name;
      return this._http.get(urlDataUserName).map(res=>res);
    }

    getGroups(){
      const urlGroups = "http://192.168.2.154:3000/groups";
      return this._http.get(urlGroups).map(res=>res);
    }

    getDataGrafico(){
      const urlDataGroups = "http://192.168.2.154:3000/dataGroups";
      return this._http.get(urlDataGroups).map(res=>res);
    }

}
