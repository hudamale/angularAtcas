import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AdminComponent } from '../../../layout/admin/admin.component';
import { DatosService } from '../default/datos.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UsersComponent implements OnInit {

  public usersUri;

  constructor(private _http: HttpClient, private _datos: DatosService, private adminLayout: AdminComponent) { }

  ngOnInit() {
    this.adminLayout.toggleOpenedExpan();
    this._datos.getUser().subscribe(
      res => {
        this.usersUri = res;
    });
  }

}
