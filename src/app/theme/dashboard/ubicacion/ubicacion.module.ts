import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UbicacionComponent } from './ubicacion.component';
import {UbicacionRoutingModule} from './ubicacion-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';
import {FormsModule} from '@angular/forms';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {AgmCoreModule} from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [  
    CommonModule,
    UbicacionRoutingModule,
    SharedModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2GoogleChartsModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk'})    
  ],
  declarations: [UbicacionComponent],
  bootstrap: [UbicacionComponent]
})
export class UbicacionModule { }
