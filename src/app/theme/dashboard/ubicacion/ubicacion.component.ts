import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AdminComponent } from '../../../layout/admin/admin.component';
import { DatosService } from '../default/datos.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-ubicacion',
  templateUrl: './ubicacion.component.html',
  styleUrls: ['./ubicacion.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UbicacionComponent implements OnInit {  

  lat = -33.453853;
  lng = -70.591912;
  latA = -33.453854;
  lngA = -70.591913;
  zoom = 50;

  styles: any = [{
    featureType: 'all',
    stylers: [{
      saturation: -80
    }]
  }, {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [{
      hue: '#00ffee'
    }, {
      saturation: 50
    }]
  }, {
    featureType: 'poi.business',
    elementType: 'labels',
    stylers: [{
      visibility: 'off'
    }]
  }];
  
  areaChartData =  {
    chartType: 'AreaChart',
    dataTable: [
      ['Year', 'Sales', 'Expenses'],
      ['2013', 1000, 400],
      ['2014', 1170, 460],
      ['2015', 660, 1120],
      ['2016', 1030, 540]
    ],
    options: {
      vAxis: { minValue: 0 },
      colors: ['#01C0C8', '#FB9678'],
      height: 300
    },
  };

    

  public seleccionCombo = new Array;
  public nameGrupo: string = "";
  public varCondi;
  public nameGruposUri;  
  private dataGraficaUri: any;

  constructor(private adminLayout: AdminComponent,
              private http: HttpClient, 
              private _datos: DatosService,             
              ) { }

  ngOnInit() {

    this.adminLayout.toggleOpenedExpan();
    var array1;
    this._datos.getDataGrafico().subscribe(
      res => {
        this.dataGraficaUri = res;
    });        
    
    setInterval(()=>
    {
      this.areaChartData =  {
        chartType: 'AreaChart',
        dataTable: [
          ['Year', 'Sales', 'Expenses'],
          ['2013', (Math.random() * (1000 - 1) + 1), (Math.random() * (1000 - 1) + 1)],
          ['2014', (Math.random() * (1000 - 1) + 1), (Math.random() * (1000 - 1) + 1)],
          ['2015', (Math.random() * (1000 - 1) + 1), (Math.random() * (1000 - 1) + 1)],
          ['2016', (Math.random() * (1000 - 1) + 1), (Math.random() * (1000 - 1) + 1)]
        ],
        options: {
          vAxis: { minValue: 0 },
          colors: ['#01C0C8', '#FB9678'],
          height: 300
        },
      };      
    },3000);
    setInterval(()=>{
      this.varCondi = Math.random() * (1000 - 1) + 1;
      if(this.varCondi>500){
        this.lat = this.lat + 0.00001;
        this.lng = this.lng + 0.00002;
      }else{
        this.lat = this.lat - 0.00002;
        this.lng = this.lng - 0.00003;
      }
      
    },1000);

    setInterval(()=>{
      this._datos.getDataGrafico().subscribe(
          res => {
            this.dataGraficaUri = res; console.log(res[2].s3_temp);
            this.data2 = {
              labels: ['admin', 'suit', 'jack.macklin', 'hans.handers', 'liam.nelson', 'supervisor_bt', 'carlos.palma'],
              datasets: [{
                label: 'Temperatura',
                backgroundColor: 'rgba(100, 221, 187, 0.52)',
                borderColor: 'rgba(72, 206, 168, 0.88)',
                pointBackgroundColor: 'rgba(51, 175, 140, 0.88)',
                pointBorderColor: 'rgba(44, 130, 105, 0.88)',
                pointHoverBackgroundColor: 'rgba(44, 130, 105, 0.88)',
                pointHoverBorderColor: 'rgba(107, 226, 193, 0.98)',
                data: [
                  (res[1].s3_temp),
                  (res[2].s3_temp), 
                  (res[3].s3_temp), 
                  (res[4].s3_temp), 
                  (res[5].s3_temp), 
                  (res[6].s3_temp), 
                  (res[7].s3_temp)
                ]
              }, {
                label: 'Gas',
                backgroundColor: 'rgba(255, 204, 189, 0.95)',
                borderColor: 'rgba(255, 165, 138, 0.95)',
                pointBackgroundColor: 'rgba(255, 116, 22, 0.94)',
                pointBorderColor: 'rgba(251, 142, 109, 0.95)',
                pointHoverBackgroundColor: 'rgba(251, 142, 109, 0.95)',
                pointHoverBorderColor: 'rgba(255, 165, 138, 0.95)',
                data: [
                  (res[1].s8_gas),
                  (res[2].s8_gas), 
                  (res[3].s8_gas), 
                  (res[4].s8_gas), 
                  (res[5].s8_gas), 
                  (res[6].s8_gas), 
                  (res[7].s8_gas)
                ]
              }]
            };
      });        
    },4000);

    this._datos.getGroups().subscribe(
      res => {
        this.nameGruposUri = res;
    });

  }  

  enableSwitch(){
    this.seleccionCombo = [];
  }

  selectChangeHandler(event: any){
    this.seleccionCombo.push(event.target.value);
    this.nameGrupo = this.nameGrupo + event.target.value + " ";
    console.log(this.seleccionCombo);
  }

  type2 = 'radar';  
  data2 = {    
    labels: [
        'admin', 
        'suit', 
        'jack.macklin', 
        'hans.handers', 
        'liam.nelson', 
        'supervisor_bt', 
        'carlos.palma'
          ],
    datasets: [
      {
        label: 'Temperatura',
        backgroundColor: 'rgba(100, 221, 187, 0.52)',
        borderColor: 'rgba(72, 206, 168, 0.88)',
        pointBackgroundColor: 'rgba(51, 175, 140, 0.88)',
        pointBorderColor: 'rgba(44, 130, 105, 0.88)',
        pointHoverBackgroundColor: 'rgba(44, 130, 105, 0.88)',
        pointHoverBorderColor: 'rgba(107, 226, 193, 0.98)',
        data: [0, 0, 0, 0, 0, 0, 0]
      }, 
      {
        label: 'Gas',
        backgroundColor: 'rgba(255, 204, 189, 0.95)',
        borderColor: 'rgba(255, 165, 138, 0.95)',
        pointBackgroundColor: 'rgba(255, 116, 22, 0.94)',
        pointBorderColor: 'rgba(251, 142, 109, 0.95)',
        pointHoverBackgroundColor: 'rgba(251, 142, 109, 0.95)',
        pointHoverBorderColor: 'rgba(255, 165, 138, 0.95)',
        data: [0, 0, 0, 0, 0, 0, 0]
      }
    ]
  };

  options2 = {
    scale: {
      reverse: true,
      ticks: {
        beginAtZero: true
      }
    }
  };

}
