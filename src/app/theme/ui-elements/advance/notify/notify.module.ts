import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyComponent } from './notify.component';
import {NotifyRoutingModule} from './notify-routing.module';
import {SharedModule} from '../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    NotifyRoutingModule,
    SharedModule
  ],
  declarations: [NotifyComponent],
  bootstrap: [NotifyComponent]
})
export class NotifyModule { }
