import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginGuard implements CanActivate, CanActivateChild {
  
  constructor(private route: Router){}

  public sessionId;

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      console.log('no');
      return true;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
    this.sessionId = localStorage.getItem('userId');
    console.log(this.sessionId);
    if(this.sessionId != "null"){
      console.log('esta logeado');
      return true;
    }else{
      console.log('no esta logeado');      
      this.route.navigate(['dashboard/analytics']);      
      return false;
    }    
  }

}

