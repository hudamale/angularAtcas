import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Administración',
    main: [
      {
        state: 'dashboard',
        short_label: 'D',
        name: 'Administración ',
        type: 'sub',
        icon: 'ti-layers',
        children: [
          {
            state: 'default',
            name: 'Compañias'
          },
          {
            state: 'employees',
            name: 'Usuarios'
          },
          {
            state: 'analytics',
            name: 'Logout'
          }
        ]        
      }      
    ],
  },
  {
    label: 'Suits',
    main: [
      {
        state: 'suit',
        short_label: 'D',
        name: 'Aura Suits ',
        type: 'sub',
        icon: 'ti-layout-grid2-alt',
        children: [
          {
            state: 'configuration',
            name: 'Configuración Aura'
          }          
        ]        
      }      
    ],
  },
  {
    label: 'Salud',
    main: [
      {
        state: 'health',
        short_label: 'D',
        name: 'Salud ',
        type: 'sub',
        icon: 'ti-crown',
        children: [
          {
            state: 'employee',
            name: 'Trabajador'
          }
        ]        
      }      
    ],
  },
  {
    label: 'Seguridad y Control',
    main: [
      {
        state: 'security',
        short_label: 'S',
        name: 'Seguridad y Control ',
        type: 'sub',
        icon: 'ti-id-badge',
        children: [
          {
            state: 'ubicacion',
            name: 'Cuadrillas'
          }
        ]        
      }      
    ],
  },
  {
    label: 'Recursos Humanos',
    main: [
      {
        state: 'resources',
        short_label: 'R',
        name: 'Recursos Humanos ',
        type: 'sub',
        icon: 'ti-user',
        children: [
          {
            state: 'resources',
            name: 'Recursos Humanos'
          },
          {
            state: 'configurationrh',
            name: 'Configuración'
          }
        ]        
      }      
    ],
  },
  {
    label: 'Management',
    main: [
      {
        state: 'management',
        short_label: 'M',
        name: 'Management ',
        type: 'sub',
        icon: 'ti-settings',
        children: [
          {
            state: 'indicators',
            name: 'Indicadores'
          },
          {
            state: 'productivity',
            name: 'Productividad'
          },
          {
            state: 'security',
            name: 'Seguridad'
          },
          {
            state: 'healthm',
            name: 'Salud'
          }
        ]        
      }      
    ],
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}
