import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';
import {MenuItems} from './shared/menu-items/menu-items';
import {BreadcrumbsComponent} from './layout/admin/breadcrumbs/breadcrumbs.component';
import {enableProdMode} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { HttpModule } from '@angular/http';
import { LoginFormComponent } from './theme/login-form/login-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginGuard } from './guard/login.guard';
import {AgmCoreModule} from '@agm/core';
import { DatosService } from './theme/dashboard/default/datos.service';
import "rxjs/add/operator/map";

enableProdMode();


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    BreadcrumbsComponent,
    LoginFormComponent
  ],
  imports: [
    BrowserModule,FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    SharedModule,
    HttpModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk'}) 
  ],
  providers: [MenuItems, LoginGuard, DatosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
